/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author aluno
 */
public class Janela extends JFrame{
    private JTextPane texto;
    public Janela(String newTexto, ServiceImpl service) {
        texto = new JTextPane();
        texto.setText(newTexto);
        texto.getDocument().addDocumentListener(new DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                System.out.println(texto.getText());
                try {
                    service.atualizarTexto(texto.getText());
                } catch (Exception ex) {
                    Logger.getLogger(Janela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                 System.out.println(texto.getText());
                try {
                    service.atualizarTexto(texto.getText());
                } catch (Exception ex) {
                    Logger.getLogger(Janela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                System.out.println("changedUpdate");
            }
            
        });
        
        this.add(texto);
        this.setTitle("Editor de Texto Colaborativo");
        this.setSize(400,300);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * @return the texto
     */
    public JTextPane getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(JTextPane texto) {
        this.texto = texto;
    }

}
