/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;
import trabalho2bimestre.Usuario;
import trabalho2bimestre.client.Service;

/**
 *
 * @author Valerio
 */
public class ServiceImpl extends UnicastRemoteObject implements Service {
    String texto;
    ArrayList<Janela> janelasClientes = new ArrayList();
    protected ServiceImpl() throws RemoteException {
        super();
    }

    private static final long serialVersionUID = 1L;

    @Override
    public String helloWorld(String nome) throws RemoteException {
        return "Vai estudar, " + nome;
    }

    @Override
    public Janela abrirJanela() throws Exception {
        Janela nova = new Janela(texto, this);
        janelasClientes.add(nova);
        return nova;
    }

    @Override
    public void atualizarTexto(String newTexto) throws Exception {
        this.texto = newTexto;
        
        for(int i=0;i<janelasClientes.size();i++) {
            janelasClientes.get(i).getTexto().setText(texto);
        }
    }
}
