/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.client;
import java.rmi.Remote;
import java.rmi.RemoteException;
import trabalho2bimestre.server.Janela;
/**
 *
 * @author Valerio
 */
public interface Service extends Remote{
    
    public String helloWorld(String nome) throws RemoteException;
    public Janela abrirJanela() throws Exception;
    public void atualizarTexto(String newTexto) throws Exception;
}
